import Vue from 'vue';
import VueRouter from 'vue-router';
import HomeView from '@/views/HomeView.vue';
import CartView from '@/views/CartView.vue';
import CategoryView from '@/views/CategoryView.vue';
import SpecialView from '@/views/SpecialView.vue';
import UserView from '@/views/UserView.vue';
import LoginView from '@/views/LoginView.vue';

Vue.use(VueRouter);

const routes = [
  { path: '/', component: LoginView, meta: { hideNavBar: true } }, // 登录页面不显示导航栏
  { path: '/user', component: UserView },
  { path: '/home', component: HomeView },
  { path: '/special', component: SpecialView },
  { path: '/category', component: CategoryView },
  { path: '/cart', component: CartView, meta: { showCart: true } }, // 设置元信息
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
