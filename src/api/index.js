//api.js
//1.导入2次封装axios
import http from "@/utils/request";
//2.获取home页面的数据,并且暴露GetHome方法
export const GetHome = () => http.get("/api/index/index");
export const GetCatalog = () => http.post("/api/catalog/index");

export const GetCurrentCatalog = (id) => http.get(`api/catalog/current?id=${id}`);
export const GetSpecial= () =>http.get("api/topic/list?page=1&size=10");
// export const GetCurrentCatalog = (id) =>
//   http.get(`api/catalog/current`, {
//     params: { id },
//   });


