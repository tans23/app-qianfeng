//1.导入vue
import Vue from "vue";
//2.导入vant组件库
import { Button } from "vant";
import { Tabbar, TabbarItem } from 'vant';
import { Search } from 'vant';
import { Swipe, SwipeItem } from 'vant';
import { Grid, GridItem } from 'vant';
import { Panel } from 'vant';
import { Image as VanImage } from 'vant';
import { Sidebar, SidebarItem } from 'vant';
import { SubmitBar } from 'vant';
import { Card } from 'vant';
import { Empty } from 'vant';

import { Checkbox, CheckboxGroup } from 'vant';
import { Stepper } from 'vant';
import { Toast } from 'vant';
Vue.use(Toast);
Vue.use(Stepper);
Vue.use(Checkbox);
Vue.use(CheckboxGroup);
Vue.use(Empty);
Vue.use(Card);
Vue.use(SubmitBar);
Vue.use(Sidebar);
Vue.use(SidebarItem);
Vue.use(VanImage);
Vue.use(Panel);
Vue.use(Grid);
Vue.use(GridItem);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Search);
Vue.use(Tabbar);
Vue.use(TabbarItem);
//3.把按钮挂在到Vue
Vue.use(Button)