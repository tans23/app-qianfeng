//1.对axios进行2次封装
import axios from "axios";
//2.创建axios的实例
const http=axios.create({
    baseURL:"", //一会再说
    timeout:4000// 4秒没响应,就断开
})
//3.配置请求拦截
http.interceptors.request.use((config)=>{
    //请求的时候,检查token
  return config
},(err)=>{
    return Promise.reject(err)
})
//4.配置响应拦截
http.interceptors.response.use((resposne)=>{
    //对返回的数据进行检查..

  return resposne
},(err)=>{
    return Promise.reject(err)
})
//5.暴露http对象
export default http;